/*
 * file nayad_sample.ino is based on the https://github.com/DFRobot/DFRobot_PH
 * Copyright   [DFRobot](http://www.dfrobot.com), 2018
 *
 * NAYAD_PH.cpp
 * version  V1.0
 * date  2021-06
 * Copyright:
 *            GNU General Public License version 3.
 * See <https://www.gnu.org/licenses/gpl-3.0.en.html> for details.
 * All above must be included in any redistribution
 *
 *
 * Calibration directions (at the serial monitor):
 *   enterph -> enter the calibration mode
 *   calph   -> calibrate with the standard buffer solution (4.0 and 7.0). They are automatically recognized
 *   exitph  -> save the calibrated parameters and exit from calibration mode
 */

#include "NAYAD_PH.h"
#include "EEPROM.h"

NAYAD_PH ph;
#define ESPADC 4096.0   //the NAYAD Analog Digital Conversion value
#define ESPVOLTAGE 3300 //the NAYAD voltage supply value
#define PH_PIN 33		//the NAYAD GPIO data pin number
float voltage, phValue, temperature = 25;

void setup()
{
	Serial.begin(115200);
	EEPROM.begin(35);//allows storage of calibration value in eeprom
	ph.begin();
}

void loop()
{
	static unsigned long timepoint = millis();
	if (millis() - timepoint > 1000U) //time interval: 1 second
	{
		timepoint = millis();
		voltage = analogRead(PH_PIN) / ESPADC * ESPVOLTAGE; // read the voltage
		Serial.print("voltage:");
		Serial.println(voltage, 4);
		
		//temperature = readTemperature();  // Remove the comment here to use data from a temperature sensor
		Serial.print("temperature:");
		Serial.print(temperature, 1);
		Serial.println("^C");

		phValue = ph.readPH(voltage, temperature); // convert voltage to pH with temperature compensation
		Serial.print("pH:");
		Serial.println(phValue, 4);
	}
	ph.calibration(voltage, temperature); // calibration process by Serial CMD
}

float readTemperature()
{
	//add your code here to get the temperature from your temperature sensor
}
