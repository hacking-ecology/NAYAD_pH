/*
 * 
 * file NAYAD_PH.cpp is based on the https://github.com/DFRobot/DFRobot_PH
 * Copyright   [DFRobot](http://www.dfrobot.com), 2018
 *
 *            GNU General Public License version 3.
 * See <https://www.gnu.org/licenses/gpl-3.0.en.html> for details.
 * All above must be included in any redistribution
 * Copyright: 2021 Hacking Ecology
 * NAYAD_PH.h
 * version  V1.0
 * date  2021-06
 * 
 * This library is developed and tested using Nayad Modular System
 *
 *
 */

#ifndef _NAYAD_PH_H_
#define _NAYAD_PH_H_

#include "Arduino.h"

#define PHVALUEADDR 0x00 //the start address of the pH calibration parameters stored in the EEPROM

#define PH_8_VOLTAGE 1122
#define PH_6_VOLTAGE 1478
#define PH_5_VOLTAGE 1654
#define PH_3_VOLTAGE 2010

#define ReceivedBufferLength 10 //length of the Serial CMD buffer

class NAYAD_PH
{
public:
    NAYAD_PH();
    ~NAYAD_PH();
    void calibration(float voltage, float temperature, char *cmd); //calibration by Serial CMD
    void calibration(float voltage, float temperature);
    float readPH(float voltage, float temperature); // voltage to pH value, with temperature compensation
    void begin();                                   //initialization

private:
    float _phValue;
    float _acidVoltage;
    float _neutralVoltage;
    float _voltage;
    float _temperature;

    char _cmdReceivedBuffer[ReceivedBufferLength]; //store the Serial CMD
    byte _cmdReceivedBufferIndex;

private:
    boolean cmdSerialDataAvailable();
    void phCalibration(byte mode); // calibration process, wirte key parameters to EEPROM
    byte cmdParse(const char *cmd);
    byte cmdParse();
};

#endif
